# Voice Collector

Web tool collect data for Speech Recognition, Keyword Spotting

## Quick links

- [Development setup](./docs/DEVELOPMENT.md)
- [Download dataset](https://github.com/common-voice/common-voice-bundler)
