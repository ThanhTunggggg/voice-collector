export const DAILY_GOALS = Object.freeze({
  speak: [100, 250],
  listen: [200],
});

export const BENEFITS = [
  'rich-data',
  'improve-audio',
  'keep-track',
  'compare-progress',
  'view-goals',
  'join-newsletter',
];

export const WHATS_PUBLIC = [
  'email-not-public',
  'recordings-and-locale-public',
  'username-optin-public',
  'demographic-deidentified-clarity-2',
  'username-email-not-demographic',
];
